import os
import pandas as pd
import mazdap as mz
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV"
    # ,
    # max_identifier_length=128,
)

conn = mz.ObieeConnector(uid='tmarx',pwd='Peneloperose1')

working_capital_std_rp = "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dealer Finance/Net Working Capital"
working_capital_std = conn.get_excel(working_capital_std_rp)
working_capital_std.iloc[:, 5:] = round(working_capital_std.iloc[:, 5:])

working_capital_std["6. SUPPLY OF RETAIL USED VEHICLES"] = (
    working_capital_std["6A. TOTAL RETAIL USED VEHICLE SALES"]
    - working_capital_std["6B. TOTAL RETAIL USED VEHICLE GP"]
    + working_capital_std["6C. TOTAL RETAIL RECONDITIONING EXPENSE"]
)

working_capital_std["7F. AVERAGE WEEK WHOLESALE USED VEHICLE UNIT SALES"] = round(
    working_capital_std["7D. TOTAL WHOLESALE USED VEHICLE UNIT SALES"] / 30 * 7
)
working_capital_std["7. SUPPLY OF WHOLESALE USED VEHICLES"] = (
    round(
        (
            (
                working_capital_std["7A. TOTAL WHOLESALE USED VEHICLE SALES"]
                - working_capital_std["7B. TOTAL WHOLESALE USED VEHICLE GP"]
            )
            / working_capital_std["7D. TOTAL WHOLESALE USED VEHICLE UNIT SALES"]
        )
    )
    * working_capital_std["7F. AVERAGE WEEK WHOLESALE USED VEHICLE UNIT SALES"]
)

working_capital_std["8. 60 DAY SUPPLY OF PARTS"] = round(
    (
        working_capital_std["8A. TOTAL PARTS SALES"]
        - working_capital_std["8B. TOTAL PARTS GROSS PROFIT"]
    )
    * 2
)

working_capital_std["14. TOTAL ASSETS REQUIREMENT"] = (
    working_capital_std["1. TOTAL OPERATING EXPENSE"]
    + working_capital_std["2. VEHICLE RECEIVABLES - TOTAL"]
    + working_capital_std["3. FINANCE & INSURANCE RECEIVABLES"]
    + working_capital_std["4. SVC, PARTS, BODY RECEIVABLES"]
    + working_capital_std["5. WARRANTY RECEIVABLES"]
    + working_capital_std["6. SUPPLY OF RETAIL USED VEHICLES"]
    + working_capital_std["7. SUPPLY OF WHOLESALE USED VEHICLES"]
    + working_capital_std["8. 60 DAY SUPPLY OF PARTS"]
    + working_capital_std["9. GAS OIL & GREASE INVENTORY"]
    + working_capital_std["10. SUBLET REPAIR INVENTORY"]
    + working_capital_std["11. WORK IN PROCESS INVENTORY"]
    + working_capital_std["12. MISC INVENTORY"]
    + working_capital_std["13. PREPAID EXPENSES"]
)

working_capital_std["15. ACCOUNTS PAYABLE"] = round(
    0.8 * working_capital_std["1. TOTAL OPERATING EXPENSE"]
)
working_capital_std["16. ACCRUED LIABILITIES"] = round(
    0.7 * working_capital_std["1. TOTAL OPERATING EXPENSE"]
)
working_capital_std["17. TOTAL CURRENT LIABILITES REQUIREMENT"] = (
    working_capital_std["15. ACCOUNTS PAYABLE"]
    + working_capital_std["16. ACCRUED LIABILITIES"]
)

working_capital_std["18. WORKING CAPITAL STANDARD"] = (
    working_capital_std["14. TOTAL ASSETS REQUIREMENT"]
    - working_capital_std["17. TOTAL CURRENT LIABILITES REQUIREMENT"]
)

working_capital_std.to_sql(
    "wp_dh_net_working_capital",
    engine,
    if_exists="append",
    index=False,
    dtype={
        "Dealer Code": String(5),
        "Dealer Name": String(100),
        "Region Code": String(2),
        "Country Code": String(2),
        "Calendar Year": String(4),
    },
)

