import datetime
import os
import subprocess
from dateutil.relativedelta import relativedelta
import mazdap as mz
import pandas as pd
from sqlalchemy import create_engine

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

conn = mz.ObieeConnector()

#### DATE ####
lastMonth = (datetime.date.today() - relativedelta(months=1)).replace(day=1)
lastMonth_dt = lastMonth.strftime("%m/%d/%Y")

dt_range = pd.date_range(start="01/01/2019", end=lastMonth_dt, freq="MS")
cal_year = dt_range.year
cal_month = dt_range.month
cal_year_month_id = cal_year.astype(str) + cal_month.map("{:02}".format)
date_master = pd.DataFrame(
    {
        "cal_date": dt_range,
        "cal_year": cal_year,
        "cal_month": cal_month,
        "cal_year_month_id": cal_year_month_id,
    }
)

date_master.to_csv("data/date_master.csv", index=False)
#### DATE ####

#### KPIs ####
kpi_mtd = pd.read_sql("SELECT * FROM RPR_STG.WP_DH_KPI_MTD", engine)
kpi_r12 = pd.read_sql("SELECT * FROM RPR_STG.WP_DH_KPI_R12", engine)
kpi_nwc = pd.read_sql("SELECT * FROM RPR_STG.WP_DH_NET_WORKING_CAPITAL", engine)

kpi_mtd.to_csv("data/kpi_mtd.csv", index=False)
kpi_r12.to_csv("data/kpi_r12.csv", index=False)
kpi_nwc.to_csv("data/kpi_nwc.csv", index=False)
#### KPIs ####

#### MASTER TABLES ####
dlr_master = conn.get_csv("/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dealer Finance/DH Dealer Master")
dlr_volume = conn.get_csv("/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dealer Finance/DH Dealer Volume")
re_stores = pd.read_sql("SELECT * FROM RPR_STG.TM_RE_STORES", engine)

dlr_master.to_csv("data/dlr_master.csv", index=False)
dlr_volume.to_csv("data/dlr_volume.csv", index=False)
re_stores.to_csv("data/re_stores.csv", index=False)
#### MASTER TABLES ####

#### SYNC ONEDRIVE ####
sync_cmd = "rclone sync -v data sharepoint:wphyo/dealerfinance"
with open("logging.txt", "w") as f:
    sync_process = subprocess.Popen(sync_cmd, shell=True, stderr=f, universal_newlines=True)
    while True:
        sync_return_code = sync_process.poll()
        if sync_return_code is not None:
            break
#### SYNC ONEDRIVE ####

