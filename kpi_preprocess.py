import os
from datetime import datetime, date

import pandas as pd
from dateutil.relativedelta import relativedelta
from sqlalchemy import create_engine
from sqlalchemy.types import DateTime, Float, Integer, String

import mazdap as mz

os.environ["NLS_LANG"] = ".AL32UTF8"

engine = create_engine(
    "oracle+cx_oracle://RPR_STG:rpSgdEv12@x4ebid10.mnao.net:1521/EDWHDEV",
    max_identifier_length=128,
)

conn = mz.ObieeConnector()

lastMonth = date.today() - relativedelta(months=1)
lastMonth_ym = int(lastMonth.strftime("%Y%m"))

report_path_mtd = "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dealer Finance/Dealer Health and Cash Flow MTD"
df_mtd = conn.get_csv(
    report_path_mtd,
    filters=mz.obiee.define_filters(
        "comparison",
        "lessOrEqual",
        '"Date"."Calendar Year Month ID"',
        {lastMonth_ym: "decimal"},
    ),
)

# convert cal year, month, year month id to str with no leading 0s
df_mtd[["Calendar Year", "Calendar Month", "Calendar Year Month ID"]] = (
    df_mtd[["Calendar Year", "Calendar Month", "Calendar Year Month ID"]]
    .astype(float)
    .astype(int)
    .astype(str)
)

df_mtd[["NET CASH ACTUAL", "WORKING CAPITAL ACTUAL"]] = df_mtd[
    ["NET CASH ACTUAL", "WORKING CAPITAL ACTUAL"]
].astype(float)



def create_date_range_filter(edate):
    end = datetime.strptime(edate, "%Y%m")
    start = end + relativedelta(months=-11)
    end_ym = int(end.strftime("%Y%m"))
    start_ym = int(start.strftime("%Y%m"))
    return mz.obiee.define_filters(
        "comparison",
        "between",
        '"Date"."Calendar Year Month ID"',
        {start_ym: "decimal", end_ym: "decimal"},
    )


report_path_r12 = "/shared/Operational Strategy/Governance and Process Innovation/WaiPhyo/Dealer Finance/Dealer Health and Cash Flow R12"
result = []
for i in df_mtd["Calendar Year Month ID"].unique():
    df_r12 = conn.get_excel(report_path_r12, filters=create_date_range_filter(i))
    df_r12["Calendar Year Month ID"] = i
    result.append(df_r12)

df_r12_final = pd.concat(result)

df_mtd.to_sql(
    "wp_dh_kpi_mtd",
    engine,
    if_exists="replace",
    index=False,
    dtype={
        "Dealer Code": String(5),
        "Dealer Name": String(100),
        "Region Code": String(2),
        "Country Code": String(2),
        "Calendar Year Month ID": String(6),
        "Calendar Year": String(4),
        "Calendar Month": String(2),
        "NET CASH ACTUAL": Float(),
        "WORKING CAPITAL ACTUAL": Float(),
    },
)

df_r12_final.to_sql(
    "wp_dh_kpi_r12",
    engine,
    if_exists="replace",
    index=False,
    dtype={
        "Dealer Code": String(5),
        "Dealer Name": String(100),
        "Region Code": String(2),
        "Country Code": String(2),
        "Calendar Year Month ID": String(6),
    },
)

