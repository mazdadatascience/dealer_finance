Order of Scripts:

Yearly  
1. net_working_cap_calc.py - Once a Year (Dec Annual)  

Monthly Scripts (Around 25th)  
1. kpi_preprocess.py - Monthly (before uploading data to sharepoint)  
2. dh_upload.py - Monthly (data sync with sharepoint)  
3. dealer_profitability.py - Monthly (data sync with sharepoint) [https://bitbucket.org/mazdadatascience/ndac_reporting/src/master/]

Monthly Power BI (Around 25th)    
[https://app.powerbi.com/groups/a0319efc-8e36-4f37-a306-e586473ae640/reports/8978fa30-bbab-4bf3-9352-5078eefa6d35/ReportSection]  
[https://app.powerbi.com/groups/a0319efc-8e36-4f37-a306-e586473ae640/reports/a68b8ad1-0bcc-4632-896a-6284b28fe2be/ReportSection94dd6cb85c0d352bbe00]  

1. Refresh Data  
2. Edit and Update filter to most recent month
3. E-mail report updates.